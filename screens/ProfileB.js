import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { View, Button, Text, Image, TouchableOpacity, TextInput, StyleSheet, ScrollView } from 'react-native'

import HomePageScreen from './HomePage'
import Profile from './Profile';

const Stack1 = createNativeStackNavigator();

function ProfileB (props) {
    return(
        <Stack1.Navigator>
            <Stack1.Screen component={ProfileBNavigation} name="ProfileB" options={{ headerShown: false }} />
        </Stack1.Navigator>
    );
}
const ProfileBNavigation = ({navigation}) => {
    return(
        <View style={styles.container}>
            <View style = {[styles.head,{flexDirection: 'row'}]}>
                <Text style = {styles.headtext}>Profile</Text>
            </View>
            
            <View style = {styles.scroll}>
                <ScrollView>
                    <View style = {styles.profpic}>
                        <TouchableOpacity style = {styles.dots}>
                            <Image
                                style={styles.prof}
                                source={require('./unknon_logo.png')}
                            />
                            <Text style = {styles.text}>@Pat</Text>
                            <Text style = {styles.texti}>1 post        144 likes</Text>
                        </TouchableOpacity>
                    </View>
                    <View style = {styles.textv}>
                        <TouchableOpacity>
                            <Image
                                style={styles.profile}
                                source={require('./unknon_logo.png')}
                            />
                        </TouchableOpacity>
                        <Text style = {styles.textp}>@Pat</Text>
                    </View>
                    <View style = {{flexDirection:'row'}}>
                        <TextInput style = {styles.input}
                            placeholder = "Hello Everyone!!"
                            placeholderTextColor = "#D7DBE0"
                            autoCapitalize = "none"
                        />
                        <View style = {styles.like}>
                            <TouchableOpacity style = {styles.dots}>
                                <Image
                                    style={styles.likeb}
                                    source={require('./like_ic.png')}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity style = {styles.dots}>
                                <Image
                                    style={styles.likeb}
                                    source={require('./comment_ic.png')}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </View>
            <View style = {styles.bg}>
                <View style = {styles.bottomView}>
                <TouchableOpacity onPress = {
                    () => navigation.navigate('HomePage')}>
                        <Image
                            style={styles.image}
                            source={require('./newsf_ic.png')}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity onPress = {
                    () => navigation.navigate('Profile')}>
                        <Image
                            style={styles.image2}
                            source={require('./unknon_logo.png')}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image
                            style={styles.image}
                            source={require('./write_ic.png')}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image
                            style={styles.image}
                            source={require('./notif_ic.png')}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

export default ProfileB

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingBottom: 23,
        backgroundColor: '#2D2951',
        alignItems: 'center',
        justifyContent: 'center',
    },
    prof: {
        width: 157,
        margin: 5,
        height: 150,
        marginHorizontal: 100
    },
    profpic: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 160,
        margin: 5,
        height: 150,
        marginHorizontal: 80,
        marginVertical: 35
    },
    image: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 60,
        margin: 5,
        height: 57,
        marginHorizontal: 15
    },
    image2: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 37,
        margin: 5,
        height: 35,
        marginTop: 15,
        marginHorizontal: 25
    },
    likeb: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 42,
        margin: 3,
        height: 40,
        marginHorizontal: 8,
        marginVertical: 6
    },
    like: {

    },
    input: {
        margin: 9,
        width: 210,
        height: 90,
        textAlign: 'center',
        fontSize: 17,
        color: 'white',
        borderColor: '#02FBAB',
        borderWidth: 5
    },
    profile : {
        backgroundColor: '#2D2951',
        width: 50,
        height: 48,
        marginRight: 220
    },
    head: {
        position: 'absolute',
        top: 50
    },
    dots : {
        marginBottom: 0
    },
    headtext: {
        fontSize: 40,
        color: 'white',
        right: 100,
        marginHorizontal: 30
    },
    text: {
        fontSize: 40,
        color: 'white',
        left: 113,
    },
    texti: {
        left: 105,
        fontSize: 20,
        color: 'white',
    },
    textv: {
        marginTop: 50,
        flexDirection: 'row'
    },
    textp: {
        fontSize: 25,
        marginTop: 8,
        marginLeft: -200,
        color: 'white',
    },
    bottomView: {
        flexDirection: 'row',
    },
    scroll: {
        height: 550,
        width: 320,
        marginTop: 160,
    },
    bg: {
        backgroundColor: '#120F35',
    }
});