import React from 'react'
import {View, Text, FlatList } from 'react-native'
import { createNativeStackNavigator } from '@react-navigation/native-stack';





class NewScreen extends React.Component{
    
   
constructor() 
{
    super();
    this.state = {
        data :[]
    }
}
componentDidMount()
    {
    this.apiCall();
    }
   async apiCall(){
        let resp = await fetch('https://ns.nowna.com/zohoTest/test/create')
        let respJson = await resp.json()
        console.warn(respJson)
        this.setState({data:respJson.response})
    }




    render() {
        return (
            <View>
                <Text> API Test </Text>
                <FlatList data = {this.state.data}
                    renderItem={({item})=> <Text> {item.title} {item.option} {item.category} </Text> } >
                </FlatList>
            </View>
        )
    }
}
export default NewScreen;