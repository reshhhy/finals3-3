import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { View, Button, Text, Image, TouchableOpacity, TextInput, StyleSheet, ScrollView } from 'react-native'

import Profile from './Profile'
import ProfileA from './ProfileA'
import ProfileB from './ProfileB'
import ProfileC from './ProfileC'
import Settings from './Settings'
import Spill from './Spill'

const Stack2 = createNativeStackNavigator();

function HomePage (props) {
    return(
        <Stack2.Navigator>
            <Stack2.Screen component={HomePageNavigation} name="Register" options={{ headerShown: false }} />
            <Stack2.Screen component={Profile} name="HomeScreen" options={{ headerShown: false }} />
        </Stack2.Navigator>
    );
}
const HomePageNavigation = ({ navigation }) => {
    return(
        <View style={styles.container}>
            <View style = {[styles.head,{flexDirection: 'row'}]}>
                <Text style = {styles.text}>UNKNON</Text>
                <TouchableOpacity onPress = {
                    () => navigation.navigate('Settings')}>
                    <Image
                        style={styles.image3}
                        source={require('./settings_ic.png')}
                    />
                </TouchableOpacity>
            </View>
            
            <View style = {styles.scroll}>
                <ScrollView>
                    <View style = {styles.textv}>
                        <TouchableOpacity onPress = {
                    () => navigation.navigate('Profile')}>
                            <Image
                                style={styles.profile}
                                source={require('./unknon_logo.png')}
                            />
                        </TouchableOpacity>
                        <Text style = {styles.textp}>@Mosh</Text>
                    </View>
                    <View style = {{flexDirection:'row'}}>
                        <TextInput style = {styles.input}
                            placeholder = "Hello Everyone!!"
                            placeholderTextColor = "#D7DBE0"
                            autoCapitalize = "none"
                        />
                        <View style = {styles.like}>
                            <TouchableOpacity style = {styles.dots}>
                                <Image
                                    style={styles.likeb}
                                    source={require('./like_ic.png')}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity style = {styles.dots}>
                                <Image
                                    style={styles.likeb}
                                    source={require('./comment_ic.png')}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style = {styles.textv}>
                        <TouchableOpacity onPress = {
                    () => navigation.navigate('ProfileA')}>
                            <Image
                                style={styles.profile}
                                source={require('./unknon_logo.png')}
                            />
                        </TouchableOpacity>
                        <Text style = {styles.textp}>@Airesh</Text>
                    </View>
                    <View style = {{flexDirection:'row'}}>
                        <TextInput style = {styles.input}
                            placeholder = "Hello Everyone!!"
                            placeholderTextColor = "#D7DBE0"
                            autoCapitalize = "none"
                        />
                        <View style = {styles.like}>
                            <TouchableOpacity style = {styles.dots}>
                                <Image
                                    style={styles.likeb}
                                    source={require('./like_ic.png')}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity style = {styles.dots}>
                                <Image
                                    style={styles.likeb}
                                    source={require('./comment_ic.png')}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style = {styles.textv}>
                        <TouchableOpacity onPress = {
                    () => navigation.navigate('ProfileB')}>
                            <Image
                                style={styles.profile}
                                source={require('./unknon_logo.png')}
                            />
                        </TouchableOpacity>
                        <Text style = {styles.textp}>@Pat</Text>
                    </View>
                    <View style = {{flexDirection:'row'}}>
                        <TextInput style = {styles.input}
                            placeholder = "Hello Everyone!!"
                            placeholderTextColor = "#D7DBE0"
                            autoCapitalize = "none"
                        />
                        <View style = {styles.like}>
                            <TouchableOpacity style = {styles.dots}>
                                <Image
                                    style={styles.likeb}
                                    source={require('./like_ic.png')}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity style = {styles.dots}>
                                <Image
                                    style={styles.likeb}
                                    source={require('./comment_ic.png')}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style = {styles.textv}>
                        <TouchableOpacity onPress = {
                    () => navigation.navigate('ProfileC')}>
                            <Image
                                style={styles.profile}
                                source={require('./unknon_logo.png')}
                            />
                        </TouchableOpacity>
                        <Text style = {styles.textp}>@Angge</Text>
                    </View>
                    <View style = {{flexDirection:'row'}}>
                        <TextInput style = {styles.input}
                            placeholder = "Hello Everyone!!"
                            placeholderTextColor = "#D7DBE0"
                            autoCapitalize = "none"
                        />
                        <View style = {styles.like}>
                            <TouchableOpacity style = {styles.dots}>
                                <Image
                                    style={styles.likeb}
                                    source={require('./like_ic.png')}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity style = {styles.dots}>
                                <Image
                                    style={styles.likeb}
                                    source={require('./comment_ic.png')}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </View>
            <View style = {styles.bg}>
                <View style = {styles.bottomView}>
                    <TouchableOpacity onPress = {
                    () => navigation.navigate('HomePage')}>
                        <Image
                            style={styles.image}
                            source={require('./newsf_ic.png')}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity onPress = {
                    () => navigation.navigate('Profile')}>
                        <Image
                            style={styles.image2}
                            source={require('./unknon_logo.png')}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity onPress = {
                    () => navigation.navigate('Spill')}>
                        <Image
                            style={styles.image}
                            source={require('./write_ic.png')}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity onPress = {
                    () => navigation.navigate('NotifAPI')}>
                        <Image
                            style={styles.image}
                            source={require('./notif_ic.png')}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

export default HomePage

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingBottom: 23,
        backgroundColor: '#2D2951',
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 60,
        margin: 5,
        height: 57,
        marginHorizontal: 15
    },
    image2: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 37,
        margin: 5,
        height: 35,
        marginTop: 15,
        marginHorizontal: 25
    },
    image3: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 73,
        margin: 5,
        height: 70,
        marginHorizontal: 15
    },
    likeb: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 42,
        margin: 3,
        height: 40,
        marginHorizontal: 8,
        marginVertical: 6
    },
    like: {

    },
    input: {
        margin: 9,
        width: 210,
        height: 90,
        textAlign: 'center',
        fontSize: 17,
        color: 'white',
        borderColor: '#02FBAB',
        borderWidth: 5
    },
    profile : {
        backgroundColor: '#2D2951',
        width: 50,
        height: 48,
        marginRight: 220
    },
    head: {
        position: 'absolute',
        top: 50
    },
    text: {
        fontSize: 40,
        color: 'white',
        marginRight: 70,
        marginTop: 10
    },
    textv: {
        flexDirection: 'row'
    },
    textp: {
        fontSize: 25,
        marginTop: 8,
        marginLeft: -200,
        color: 'white',
    },
    bottomView: {
        flexDirection: 'row',
    },
    scroll: {
        height: 550,
        width: 320,
        marginTop: 160,
    },
    bg: {
        backgroundColor: '#120F35',
    }
});