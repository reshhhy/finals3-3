import React from 'react'
import { View, Text, TouchableOpacity, Image, Button, TextInput, StyleSheet} from 'react-native'

const Home = ({ navigation }) => {
    return(
        <View style={styles.container}>
            <Image
                style={styles.image}
                source={require('./unknon_logo.png')}
            />
            <Text style = {styles.text}>Log in</Text>
            <TextInput style = {styles.input}
                placeholder = "Username"
                placeholderTextColor = "#D7DBE0"
                autoCapitalize = "none"
            />
            <TextInput style = {styles.input}
                placeholder = "Password"
                placeholderTextColor = "#D7DBE0"
                autoCapitalize = "none"
            />
            <Text style = {styles.texta}>Remember Me    Forgot Password?</Text>
            <TouchableOpacity
                style = {styles.submitButton}
                onPress = {
                    () => navigation.navigate('HomePage')}>
                
                <Text style = {styles.submitButtonText}> Sign in </Text>
            </TouchableOpacity>
            <TouchableOpacity
                style = {styles.registerButton}
                onPress = {
                    () => navigation.navigate('Register')}>
                
                <Text style = {styles.registerButtonText}> Create Account </Text>
            </TouchableOpacity>
            <Text style = {styles.texta}>Do you have an account?</Text>
            
        </View>
    )
}

export default Home

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingBottom: 23,
        backgroundColor: '#2D2951',
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        width: 80,
        resizeMode: 'contain',
        paddingRight: 50,
        height: 80,
        position: 'absolute',
        top: 120,
        bottom: 0,
        right: 0,
        left: 60
      },
      text: {
        fontSize: 50,
        color: 'white',
        marginBottom: 30,
        paddingLeft: 69
      },
      texta: {
        marginTop: 5,
        fontSize: 14,
        color: 'white',
      },
      input: {
        margin: 9,
        width: 210,
        height: 50,
        textAlign: 'center',
        fontSize: 17,
        color: 'white',
        borderColor: '#02FBAB',
        borderWidth: 5
     },
      submitButton: {
        backgroundColor: '#1DC48F',
        borderRadius: 9,
        paddingLeft: 20,
        paddingTop: 9,
        margin: 11,
        width: 120,
        height: 44,
        },
        submitButtonText:{
          fontSize: 24,
          color: 'white'
       },
       registerButton: {
        backgroundColor: '#5C3685',
        borderRadius: 9,
        paddingLeft: 7,
        paddingTop: 14,
        margin: 6,
        width: 120,
        height: 44,
        },
        registerButtonText:{
          fontSize: 16,
          color: 'white'
       }
});