import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { View, Button, Text, Image, TouchableOpacity, TextInput, StyleSheet, ScrollView } from 'react-native'

import HomePageScreen from './HomePage'
import Profile from './Profile';
import HomeScreen from './Home'

const Stack1 = createNativeStackNavigator();

function Settings (props) {
    return(
        <Stack1.Navigator>
            <Stack1.Screen component={SettingsNavigation} name="Settings" options={{ headerShown: false }} />
        </Stack1.Navigator>
    );
}
const SettingsNavigation = ({navigation}) => {
    return(
        <View style={styles.container}>
            <View style = {[styles.head,{flexDirection: 'row'}]}>
                <Text style = {styles.headtext}>Settings</Text>
            </View>
            <View style = {styles.adjusts}>
                <View style = {styles.textv}>
                    <TouchableOpacity>
                        <Image
                            style={styles.profile1}
                            source={require('./unknon_logo.png')}
                        />
                    </TouchableOpacity>
                    <Text style = {styles.textab}>About</Text>
                </View>
                <View style = {styles.textv}>
                    <TouchableOpacity>
                        <Image
                            style={styles.profile}
                            source={require('./security_ic.png')}
                        />
                    </TouchableOpacity>
                    <Text style = {styles.textp}>Security</Text>
                </View>
                <View style = {styles.textv}>
                    <TouchableOpacity>
                        <Image
                            style={styles.profile}
                            source={require('./update_ic.png')}
                        />
                    </TouchableOpacity>
                    <Text style = {styles.textp}>Check for Update</Text>
                </View>
                <View style = {styles.textv}>
                    <TouchableOpacity onPress = {
                    () => navigation.navigate('Home')}>
                        <Image
                            style={styles.profile}
                            source={require('./logout_ic.png')}
                        />
                    </TouchableOpacity>
                    <Text style = {styles.textp}>Sign out</Text>
                </View>
            </View>
        </View>
    )
}

export default Settings

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingBottom: 23,
        backgroundColor: '#2D2951',
        alignItems: 'center',
        justifyContent: 'center',
    },
    prof: {
        width: 157,
        margin: 5,
        height: 150,
        marginHorizontal: 100
    },
    profpic: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 160,
        margin: 5,
        height: 150,
        marginHorizontal: 80,
        marginVertical: 35
    },
    image: {
        width: 60,
        margin: 5,
        height: 57,
        marginHorizontal: 15
    },
    image2: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 37,
        margin: 5,
        height: 35,
        marginTop: 15,
        marginHorizontal: 25
    },
    likeb: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 42,
        margin: 3,
        height: 40,
        marginHorizontal: 8,
        marginVertical: 6
    },
    like: {

    },
    input: {
        margin: 9,
        width: 210,
        height: 90,
        textAlign: 'center',
        fontSize: 17,
        color: 'white',
        borderColor: '#02FBAB',
        borderWidth: 5
    },
    profile : {
        backgroundColor: '#2D2951',
        width: 70,
        height: 70,
        marginRight: 220
    },
    profile1 : {
        backgroundColor: '#2D2951',
        width: 50,
        height: 48,
        left: 13,
        marginRight: 240
    },
    head: {
        position: 'absolute',
        top: 53
    },
    dots : {
        marginBottom: 0
    },
    headtext: {
        fontSize: 40,
        color: 'white',
        right: 100,
        marginHorizontal: 30
    },
    text: {
        fontSize: 40,
        color: 'white',
        left: 113,
    },
    texti: {
        left: 105,
        fontSize: 20,
        color: 'white',
    },
    textv: {
        marginTop: 30,
        flexDirection: 'row'
    },
    textab: {
        fontSize: 25,
        marginTop: 8,
        marginLeft: -200,
        color: 'white',
    },
    adjusts: {
        marginBottom: 120,
        marginRight: 40
    },
    textp: {
        fontSize: 25,
        marginTop: 20,
        marginLeft: -200,
        color: 'white',
    }
});