import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { View, Text, TouchableOpacity, Image, Button, TextInput, StyleSheet } from 'react-native'

import HomeScreen from './Home'

const Stack1 = createNativeStackNavigator();

function Register (props) {
    return(
        <Stack1.Navigator>
            <Stack1.Screen component={RegisterNavigation} name="Register" options={{ headerShown: false }} />
            <Stack1.Screen component={HomeScreen} name="HomeScreen" options={{ headerShown: false }} />
        </Stack1.Navigator>
    );
}

const RegisterNavigation = ({navigation}) => {
    return(
        <View style={styles.container}>
            <Image
                style={styles.image}
                source={require('./unknon_logo.png')}
            />
            <Text style = {styles.text}>Sign Up</Text>
            <TextInput style = {styles.input}
                placeholder = "Email"
                placeholderTextColor = "#D7DBE0"
                autoCapitalize = "none"
            />
            <TextInput style = {styles.input}
                placeholder = "Username"
                placeholderTextColor = "#D7DBE0"
                autoCapitalize = "none"
            />
            <TextInput style = {styles.input}
                placeholder = "Password"
                placeholderTextColor = "#D7DBE0"
                autoCapitalize = "none"
            />
            <TextInput style = {styles.input}
                placeholder = "Re-enter password"
                placeholderTextColor = "#D7DBE0"
                autoCapitalize = "none"
            />
            <TouchableOpacity
                style = {styles.signupButton}
                onPress = {
                    () => navigation.navigate('Home')}>
                <Text style = {styles.signupButtonText}> Register </Text>
            </TouchableOpacity>
        </View>
    )
}

export default Register

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingBottom: 23,
        backgroundColor: '#2D2951',
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        width: 60,
        resizeMode: 'contain',
        paddingRight: 50,
        height: 60,
        position: 'absolute',
        top: 120,
        bottom: 0,
        right: 0,
        left: 80
    },
    text: {
        fontSize: 39,
        color: 'white',
        marginBottom: 35,
        paddingLeft: 70
    },
    input: {
        margin: 9,
        width: 210,
        height: 50,
        textAlign: 'center',
        fontSize: 17,
        color: 'white',
        borderColor: '#02FBAB',
        borderWidth: 5
     },
     signupButton: {
        backgroundColor: '#5C3685',
        borderRadius: 9,
        paddingLeft: 7,
        paddingTop: 10,
        margin: 6,
        width: 120,
        height: 44,
    },
    signupButtonText:{
        fontSize: 24,
        top: 0,
        bottom: 10,
        right: 0,
        left: 9,
        color: 'white'
    }
});