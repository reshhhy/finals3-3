import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { View, Button, Text, Image, TouchableOpacity, TextInput, StyleSheet, ScrollView } from 'react-native'

import HomePageScreen from './HomePage'

const Stack1 = createNativeStackNavigator();

function Spill (props) {
    return(
        <Stack1.Navigator>
            <Stack1.Screen component={SpillNavigation} name="Spill" options={{ headerShown: false }} />
        </Stack1.Navigator>
    );
}

const SpillNavigation = ({navigation}) => {
    return(
        <View style={styles.container}>
            <View style = {[styles.head,{flexDirection: 'row'}]}>
                <Text style = {styles.headtext}>Post</Text>
            </View>
            
            <View style = {styles.scroll}>
                <ScrollView>
                    <View style = {styles.textv}>
                        <TouchableOpacity>
                            <Image
                                style={styles.profile}
                                source={require('./unknon_logo.png')}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style = {{flexDirection:'row'}}>
                        <TextInput style = {styles.input}
                            placeholder = "Spill the tea.."
                            placeholderTextColor = "#ffffff"
                            autoCapitalize = "none"
                        />
                    </View>
                    <TouchableOpacity
                        style = {styles.signupButton}>
                        <Text style = {styles.signupButtonText}> Spill </Text>
                    </TouchableOpacity>
                </ScrollView>
            </View>
            <View style = {styles.bg}>
                <View style = {styles.bottomView}>
                    <TouchableOpacity onPress = {
                    () => navigation.navigate('HomePage')}>
                        <Image
                            style={styles.image}
                            source={require('./newsf_ic.png')}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image
                            style={styles.image2}
                            source={require('./unknon_logo.png')}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image
                            style={styles.image}
                            source={require('./write_ic.png')}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image
                            style={styles.image}
                            source={require('./notif_ic.png')}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

export default Spill

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingBottom: 23,
        backgroundColor: '#2D2951',
        alignItems: 'center',
        justifyContent: 'center',
    },
    prof: {
        width: 157,
        margin: 5,
        height: 150,
        marginHorizontal: 100
    },
    profpic: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 160,
        margin: 5,
        height: 150,
        marginHorizontal: 80,
        marginVertical: 35
    },
    signupButton: {
        backgroundColor: '#5C3685',
        borderRadius: 9,
        paddingLeft: 23,
        paddingTop: 10,
        margin: 6,
        marginLeft: 200,
        width: 120,
        height: 44,
    },
    signupButtonText:{
        fontSize: 24,
        top: 0,
        bottom: 10,
        right: 0,
        left: 9,
        color: 'white'
    },
    image: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 60,
        margin: 5,
        height: 57,
        marginHorizontal: 15
    },
    image2: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 37,
        margin: 5,
        height: 35,
        marginTop: 15,
        marginHorizontal: 25
    },
    likeb: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 42,
        margin: 3,
        height: 40,
        marginHorizontal: 8,
        marginVertical: 6
    },
    like: {

    },
    input: {
        margin: 9,
        width: 260,
        height: 234,
        textAlign: 'center',
        fontSize: 17,
        color: 'white',
        borderColor: '#02FBAB',
        borderWidth: 5
    },
    profile : {
        backgroundColor: '#2D2951',
        width: 50,
        height: 48,
        marginRight: 220
    },
    head: {
        position: 'absolute',
        top: 50
    },
    dots : {
        marginBottom: 0
    },
    headtext: {
        fontSize: 40,
        color: 'white',
        right: 100,
        marginHorizontal: 30
    },
    text: {
        fontSize: 40,
        color: 'white',
        left: 113,
    },
    texti: {
        left: 105,
        fontSize: 20,
        color: 'white',
    },
    textv: {
        marginTop: 10,
        flexDirection: 'row'
    },
    textp: {
        fontSize: 25,
        marginTop: 8,
        marginLeft: -200,
        color: 'white',
    },
    bottomView: {
        flexDirection: 'row',
    },
    scroll: {
        height: 550,
        width: 320,
        marginTop: 160,
    },
    bg: {
        backgroundColor: '#120F35',
    }
});