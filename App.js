import React, { Component } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import HomeScreen from './screens/Home'
import HomePageScreen from './screens/HomePage'
import RegisterScreen from './screens/Register';
import Profile from './screens/Profile';
import ProfileA from './screens/ProfileA';
import ProfileB from './screens/ProfileB';
import ProfileC from './screens/ProfileC';
import Settings from './screens/Settings';
import Spill from './screens/Spill';
import NotifAPI from './screens/NotifAPI';

const Stack = createNativeStackNavigator();

const MyStack = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{ title: 'Unknon' }}
        />
        <Stack.Screen 
          name="HomePage" 
          component={HomePageScreen}
          options={{ headerShown: false }}
        /> 
        <Stack.Screen 
          name="Register"
          component={RegisterScreen} 
        />
        <Stack.Screen 
        component={Profile} 
        name="Profile" 
        options={{ headerShown: false }} />
        <Stack.Screen 
          component={ProfileA} 
          name="ProfileA" 
          options={{ headerShown: false }} />
        <Stack.Screen 
          component={ProfileB} 
          name="ProfileB" 
          options={{ headerShown: false }} />
        <Stack.Screen 
          component={ProfileC} 
          name="ProfileC" 
          options={{ headerShown: false }} />
        <Stack.Screen 
        component={Settings} 
        name="Settings" 
        options={{ headerShown: false }} />
        <Stack.Screen 
        component={Spill} 
        name="Spill" 
        options={{ headerShown: false }} />
        <Stack.Screen 
        component={NotifAPI} 
        name="NotifAPI" 
        options={{ headerShown: false }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default MyStack